package com.iramml.ejerciciovenues.usecase

import com.iramml.ejerciciovenues.data.repository.PlacesRepository
import io.reactivex.Maybe

class GetFavoritePlaceStatusUseCase(
    private val placesRepository: PlacesRepository
) {

    fun invoke(placeId: String): Maybe<Boolean> =
        placesRepository.getFavoritePlaceStatus(placeId)
}