package com.iramml.ejerciciovenues.usecase

import com.google.android.gms.maps.model.LatLng
import com.iramml.ejerciciovenues.data.repository.PlacesRepository
import com.iramml.ejerciciovenues.domain.Place
import io.reactivex.Flowable
import io.reactivex.Single

class GetAllFavoritePlacesUseCase (
    private val placesRepository: PlacesRepository
) {
    fun invoke(): Flowable<List<Place>> =
        placesRepository.getAllFavoritePlaces()
}