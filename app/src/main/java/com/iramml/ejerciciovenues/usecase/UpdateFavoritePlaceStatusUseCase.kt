package com.iramml.ejerciciovenues.usecase

import com.iramml.ejerciciovenues.data.repository.PlacesRepository
import com.iramml.ejerciciovenues.domain.Place
import io.reactivex.Maybe

class UpdateFavoritePlaceStatusUseCase(
    private val placesRepository: PlacesRepository
) {

    fun invoke(place: Place): Maybe<Boolean> =
        placesRepository.updateFavoritePlaceStatus(place)
}