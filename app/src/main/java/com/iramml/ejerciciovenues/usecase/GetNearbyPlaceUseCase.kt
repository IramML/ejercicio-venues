package com.iramml.ejerciciovenues.usecase

import com.google.android.gms.maps.model.LatLng
import com.iramml.ejerciciovenues.data.repository.PlacesRepository
import com.iramml.ejerciciovenues.domain.Place
import io.reactivex.Single

class GetNearbyPlaceUseCase(
    private val placesRepository: PlacesRepository
) {
    fun invoke(latLng: LatLng): Single<List<Place>> =
        placesRepository.getNearbyPlaces(latLng)
}