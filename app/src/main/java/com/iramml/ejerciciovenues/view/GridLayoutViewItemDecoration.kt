package com.iramml.ejerciciovenues.view

import android.graphics.Rect
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView.ItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.iramml.ejerciciovenues.view.GridLayoutViewItemDecoration
import com.iramml.ejerciciovenues.view.LinearLayoutViewItemDecoration

internal object GridLayoutViewItemDecoration {
    fun getItemOffsets(
        outRect: Rect,
        gridLayoutManager: GridLayoutManager,
        position: Int,
        itemCount: Int,
        space: Int
    ) {
        val cols = gridLayoutManager.spanCount
        val rows = Math.ceil(itemCount / cols.toDouble()).toInt()
        outRect.top = space
        outRect.left = space
        if (position % cols == cols - 1) {
            outRect.right = space
        } else {
            outRect.right = 0
        }
        if (position / cols == rows - 1) {
            outRect.bottom = space
        } else {
            outRect.bottom = 0
        }
    }
}