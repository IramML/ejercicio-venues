package com.iramml.ejerciciovenues

import androidx.appcompat.app.AppCompatDelegate
import android.app.Application


class App: Application() {

    override fun onCreate() {
        super.onCreate()

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
    }
}