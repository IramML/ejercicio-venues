package com.iramml.ejerciciovenues.data.datasource

import com.google.android.gms.maps.model.LatLng
import com.iramml.ejerciciovenues.domain.Place
import io.reactivex.Single

interface APIPlacesDataSource {
    fun getNearbyPlaces(location: LatLng): Single<List<Place>>
}