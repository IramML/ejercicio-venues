package com.iramml.ejerciciovenues.data.repository

import com.google.android.gms.maps.model.LatLng
import com.iramml.ejerciciovenues.data.datasource.APIPlacesDataSource
import com.iramml.ejerciciovenues.data.datasource.LocalPlacesDataSource
import com.iramml.ejerciciovenues.domain.Place
import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Single

class PlacesRepository(
    private val apiPlacesDataSource: APIPlacesDataSource,
    private val localPlacesDataSource: LocalPlacesDataSource,
) {
    fun getNearbyPlaces(location: LatLng): Single<List<Place>> =
        apiPlacesDataSource.getNearbyPlaces(location)

    fun getAllFavoritePlaces(): Flowable<List<Place>> =
        localPlacesDataSource.getAllFavoritePlaces()

    fun getFavoritePlaceStatus(placeId: String): Maybe<Boolean> =
        localPlacesDataSource.getFavoritePlaceStatus(placeId)

    fun updateFavoritePlaceStatus(place: Place): Maybe<Boolean> =
        localPlacesDataSource.updateFavoritePlaceStatus(place)
}