package com.iramml.ejerciciovenues.data.datasource

import com.iramml.ejerciciovenues.domain.Place
import io.reactivex.Flowable
import io.reactivex.Maybe

interface LocalPlacesDataSource {
    fun getAllFavoritePlaces(): Flowable<List<Place>>

    fun getFavoritePlaceStatus(placeId: String): Maybe<Boolean>

    fun updateFavoritePlaceStatus(place: Place): Maybe<Boolean>
}