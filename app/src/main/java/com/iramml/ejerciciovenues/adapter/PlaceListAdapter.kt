package com.iramml.ejerciciovenues.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.iramml.ejerciciovenues.R
import com.iramml.ejerciciovenues.databinding.ItemListPlaceBinding
import com.iramml.ejerciciovenues.domain.Place
import com.iramml.ejerciciovenues.util.bindingInflate

class PlaceListAdapter(
    private val listener: (Place) -> Unit
): RecyclerView.Adapter<PlaceListAdapter.PlaceListViewHolder>() {
    private val placeList: MutableList<Place> = mutableListOf()

    fun updateData(newData: List<Place>) {
        placeList.clear()
        placeList.addAll(newData)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        PlaceListViewHolder(
            parent.bindingInflate(R.layout.item_list_place, false),
            listener
        )

    override fun getItemCount() = placeList.size

    override fun onBindViewHolder(holder: PlaceListViewHolder, position: Int) {
        holder.bind(placeList[position])
    }

    class PlaceListViewHolder(
        private val dataBinding: ItemListPlaceBinding,
        private val listener: (Place) -> Unit
    ): RecyclerView.ViewHolder(dataBinding.root) {

        fun bind(item: Place){
            dataBinding.tvPlaceName.text = item.name
            itemView.setOnClickListener { listener(item) }
        }

    }
}