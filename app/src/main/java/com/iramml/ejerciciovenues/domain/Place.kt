package com.iramml.ejerciciovenues.domain

data class Place(
    val id: String,
    val name: String,
    val lat: Double,
    val lng: Double,
)
