package com.iramml.ejerciciovenues.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.iramml.ejerciciovenues.R
import com.iramml.ejerciciovenues.data.datasource.APIPlacesDataSource
import com.iramml.ejerciciovenues.data.datasource.LocalPlacesDataSource
import com.iramml.ejerciciovenues.data.repository.PlacesRepository
import com.iramml.ejerciciovenues.database.PlaceDatabase
import com.iramml.ejerciciovenues.database.datasource.DatabasePlacesDataSource
import com.iramml.ejerciciovenues.databinding.ActivityVenueDetailBinding
import com.iramml.ejerciciovenues.domain.Place
import com.iramml.ejerciciovenues.parcelable.PlaceParcelable
import com.iramml.ejerciciovenues.parcelable.toPlaceDomain
import com.iramml.ejerciciovenues.presentation.PlaceDetailViewModel
import com.iramml.ejerciciovenues.presentation.util.Event
import com.iramml.ejerciciovenues.retrofit.APIConstants
import com.iramml.ejerciciovenues.retrofit.datasource.RetrofitPlacesDataSource
import com.iramml.ejerciciovenues.retrofit.request.PlacesRequest
import com.iramml.ejerciciovenues.usecase.GetFavoritePlaceStatusUseCase
import com.iramml.ejerciciovenues.usecase.UpdateFavoritePlaceStatusUseCase
import com.iramml.ejerciciovenues.util.Constants
import com.iramml.ejerciciovenues.util.getViewModel
import com.iramml.ejerciciovenues.util.showLongToast

class VenueDetailActivity : AppCompatActivity(), OnMapReadyCallback {
    private var place: Place? = null
    private var mMap: GoogleMap? = null
    private lateinit var binding: ActivityVenueDetailBinding


    private val localPlaceDataSource: LocalPlacesDataSource by lazy {
        DatabasePlacesDataSource(PlaceDatabase.getDatabase(applicationContext))
    }

    private val placesRequest: PlacesRequest by lazy {
        PlacesRequest(APIConstants.BASE_API_URL)
    }

    private val remotePlacesDataSource: APIPlacesDataSource by lazy {
        RetrofitPlacesDataSource(placesRequest)
    }

    private val placeRepository: PlacesRepository by lazy {
        PlacesRepository(remotePlacesDataSource, localPlaceDataSource)
    }

    private val getFavoritePlaceStatusUseCase: GetFavoritePlaceStatusUseCase by lazy {
        GetFavoritePlaceStatusUseCase(placeRepository)
    }

    private val updateFavoritePlaceStatusUseCase: UpdateFavoritePlaceStatusUseCase by lazy {
        UpdateFavoritePlaceStatusUseCase(placeRepository)
    }

    private val placeDetailViewModel: PlaceDetailViewModel by lazy {
        getViewModel {
            PlaceDetailViewModel(
                intent.getParcelableExtra<PlaceParcelable>(Constants.EXTRA_PLACE)?.toPlaceDomain(),
                getFavoritePlaceStatusUseCase,
                updateFavoritePlaceStatusUseCase,
            )
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityVenueDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        binding.faFavorite.setOnClickListener { placeDetailViewModel.onUpdateFavoritePlaceStatus() }

        placeDetailViewModel.placeValues.observe(this, Observer(this::loadPlace))
        placeDetailViewModel.isFavorite.observe(this, Observer(this::updateFavoriteIcon))
        placeDetailViewModel.events.observe(this, Observer(this::validateEvents))

        placeDetailViewModel.onPlaceValidation()

        actionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun loadPlace(place: Place){
        this.place = place
        if (mMap != null) {
            showPlaceData()
        }

    }

    private fun updateFavoriteIcon(isFavorite: Boolean?){
        binding.faFavorite.setImageResource(
            if (isFavorite != null && isFavorite) {
                R.drawable.ic_baseline_favorite_24
            } else {
                R.drawable.ic_baseline_favorite_border_24
            }
        )
    }

    private fun validateEvents(event: Event<PlaceDetailViewModel.PlaceDetailNavigation>?) {
        event?.getContentIfNotHandled()?.let { navigation ->
            when (navigation) {

                PlaceDetailViewModel.PlaceDetailNavigation.CloseActivity -> {
                    this@VenueDetailActivity.showLongToast("Error al cargar el lugar")
                    finish()
                }
            }
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        if (place != null) {
            showPlaceData()
        }
    }

    private fun showPlaceData() {
        val location = LatLng(place!!.lat, place!!.lng)
        mMap!!.addMarker(MarkerOptions().position(location).title(place!!.name))
        mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 16f))

        binding.tvVenueName.text = place!!.name
    }
}