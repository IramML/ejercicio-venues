package com.iramml.ejerciciovenues.activity

import android.os.Bundle
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.iramml.ejerciciovenues.R
import com.iramml.ejerciciovenues.activity.ui.favorites.FavoritesFragment
import com.iramml.ejerciciovenues.activity.ui.home.HomeFragment
import com.iramml.ejerciciovenues.databinding.ActivityHomeBinding
import com.iramml.ejerciciovenues.domain.Place
import com.iramml.ejerciciovenues.parcelable.toPlaceParcelable
import com.iramml.ejerciciovenues.util.Constants
import com.iramml.ejerciciovenues.util.startActivity

class HomeActivity : AppCompatActivity(), HomeFragment.OnPlaceListFragmentListener,
    FavoritesFragment.OnFavoriteListFragmentListener {

    private lateinit var binding: ActivityHomeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val navView: BottomNavigationView = binding.navView

        val navController = findNavController(R.id.nav_host_fragment_activity_home)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_home, R.id.navigation_favorites
            )
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

    }

    override fun openPlaceDetail(place: Place) {
        startActivity<VenueDetailActivity> {
            putExtra(Constants.EXTRA_PLACE, place.toPlaceParcelable())
        }
    }

}