package com.iramml.ejerciciovenues.activity.ui.home

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.iramml.ejerciciovenues.R
import com.iramml.ejerciciovenues.adapter.PlaceListAdapter
import com.iramml.ejerciciovenues.data.datasource.APIPlacesDataSource
import com.iramml.ejerciciovenues.data.datasource.LocalPlacesDataSource
import com.iramml.ejerciciovenues.data.repository.PlacesRepository
import com.iramml.ejerciciovenues.database.PlaceDatabase
import com.iramml.ejerciciovenues.database.datasource.DatabasePlacesDataSource
import com.iramml.ejerciciovenues.databinding.FragmentHomeBinding
import com.iramml.ejerciciovenues.domain.Place
import com.iramml.ejerciciovenues.presentation.HomeViewModel
import com.iramml.ejerciciovenues.presentation.PlacesViewModel
import com.iramml.ejerciciovenues.presentation.util.Event
import com.iramml.ejerciciovenues.retrofit.APIConstants
import com.iramml.ejerciciovenues.retrofit.datasource.RetrofitPlacesDataSource
import com.iramml.ejerciciovenues.retrofit.request.PlacesRequest
import com.iramml.ejerciciovenues.usecase.GetNearbyPlaceUseCase
import com.iramml.ejerciciovenues.util.getViewModel
import com.iramml.ejerciciovenues.util.setItemDecorationSpacing
import com.iramml.ejerciciovenues.util.showLongToast
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.karumi.dexter.listener.single.PermissionListener


class HomeFragment : Fragment(), OnMapReadyCallback {
    private var mMap: GoogleMap? = null
    private var _binding: FragmentHomeBinding? = null
    lateinit var homeViewModel: HomeViewModel
    private lateinit var placeListAdapter: PlaceListAdapter
    private lateinit var listener: OnPlaceListFragmentListener

    private val placesRequest: PlacesRequest by lazy {
        PlacesRequest(APIConstants.BASE_API_URL)
    }

    private val remotePlacesDataSource: APIPlacesDataSource by lazy {
        RetrofitPlacesDataSource(placesRequest)
    }

    private val localPlaceDataSource: LocalPlacesDataSource by lazy {
        DatabasePlacesDataSource(PlaceDatabase.getDatabase(requireContext()))
    }

    private val placesRepository: PlacesRepository by lazy {
        PlacesRepository(remotePlacesDataSource, localPlaceDataSource)
    }

    private val getNearbyPlacesUseCase: GetNearbyPlaceUseCase by lazy {
        GetNearbyPlaceUseCase(placesRepository)
    }

    private val placesListViewModel: PlacesViewModel by lazy {
        getViewModel { PlacesViewModel(getNearbyPlacesUseCase) }
    }

    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        homeViewModel =
            ViewModelProvider(this).get(HomeViewModel::class.java)
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root

        val mapFragment = childFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        placeListAdapter = PlaceListAdapter { place ->
            listener.openPlaceDetail(place)
        }.also {
            setHasOptionsMenu(true)
        }
        binding.rvPlaces.run{
            setItemDecorationSpacing(resources.getDimension(R.dimen.list_item_padding))
            adapter = placeListAdapter
        }

        homeViewModel.initializeLocation(requireActivity())
        Dexter.withContext(requireContext())
            .withPermissions(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
            .withListener(object : MultiplePermissionsListener {
                @SuppressLint("MissingPermission")
                override fun onPermissionsChecked(p0: MultiplePermissionsReport?) {
                    homeViewModel.startLocationTracking()
                    if (mMap != null) {
                        mMap!!.isMyLocationEnabled = true
                    }

                }

                override fun onPermissionRationaleShouldBeShown(
                    p0: MutableList<PermissionRequest>?,
                    p1: PermissionToken?
                ) {
                }

            }).check()


        homeViewModel.getCurrentLocation()?.observe(viewLifecycleOwner) {
            if (it != null) {
                placesListViewModel.onGetNearbyPlaces(it)
                homeViewModel.stopLocationTracking()
                val update: CameraUpdate = CameraUpdateFactory.newLatLngZoom(it, 16f)
                mMap?.animateCamera(update)
            }
        }

        placesListViewModel.events.observe(viewLifecycleOwner, Observer(this::validateEvents))
    }


    private fun validateEvents(event: Event<PlacesViewModel.PlacesListNavigation>?) {
        event?.getContentIfNotHandled()?.let { navigation ->
            when(navigation) {
                is PlacesViewModel.PlacesListNavigation.ShowPlacesError -> navigation.run {
                    context?.showLongToast("Error -> ${error.message}")
                }
                is PlacesViewModel.PlacesListNavigation.ShowPlacesList -> navigation.run {
                    placeListAdapter.updateData(placesList)
                    mMap?.clear()
                    placesList.forEach {
                        mMap?.addMarker(MarkerOptions().position(LatLng(it.lat, it.lng)).title(it.name))
                    }
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        if (!(ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
                    )) {
            mMap!!.isMyLocationEnabled = true
        }

    }

    override fun onStop() {
        super.onStop()
        homeViewModel.stopLocationTracking()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try{
            listener = context as OnPlaceListFragmentListener
        }catch (e: ClassCastException){
            throw ClassCastException("$context must implement OnPlaceListFragmentListener")
        }
    }

    interface OnPlaceListFragmentListener {
        fun openPlaceDetail(place: Place)
    }

    companion object {

        fun newInstance(args: Bundle? = Bundle()) = HomeFragment().apply {
            arguments = args
        }
    }
}