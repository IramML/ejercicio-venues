package com.iramml.ejerciciovenues.activity.ui.favorites

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.iramml.ejerciciovenues.R
import com.iramml.ejerciciovenues.adapter.PlaceListAdapter
import com.iramml.ejerciciovenues.data.datasource.APIPlacesDataSource
import com.iramml.ejerciciovenues.data.datasource.LocalPlacesDataSource
import com.iramml.ejerciciovenues.data.repository.PlacesRepository
import com.iramml.ejerciciovenues.database.PlaceDatabase
import com.iramml.ejerciciovenues.database.datasource.DatabasePlacesDataSource
import com.iramml.ejerciciovenues.databinding.FragmentFavoritesBinding
import com.iramml.ejerciciovenues.domain.Place
import com.iramml.ejerciciovenues.presentation.FavoritesViewModel
import com.iramml.ejerciciovenues.presentation.util.Event
import com.iramml.ejerciciovenues.retrofit.APIConstants.BASE_API_URL
import com.iramml.ejerciciovenues.retrofit.datasource.RetrofitPlacesDataSource
import com.iramml.ejerciciovenues.retrofit.request.PlacesRequest
import com.iramml.ejerciciovenues.usecase.GetAllFavoritePlacesUseCase
import com.iramml.ejerciciovenues.util.getViewModel
import com.iramml.ejerciciovenues.util.setItemDecorationSpacing

class FavoritesFragment : Fragment() {
    private var _binding: FragmentFavoritesBinding? = null

    private lateinit var listener: OnFavoriteListFragmentListener

    private val placesRequest: PlacesRequest by lazy {
        PlacesRequest(BASE_API_URL)
    }

    private val localPlacesDataSource: LocalPlacesDataSource by lazy {
        DatabasePlacesDataSource(PlaceDatabase.getDatabase(requireContext()))
    }

    private val remotePlacesDataSource: APIPlacesDataSource by lazy {
        RetrofitPlacesDataSource(placesRequest)
    }

    private val placesRepository: PlacesRepository by lazy {
        PlacesRepository(remotePlacesDataSource, localPlacesDataSource)
    }

    private val getAllFavoritePlacesUseCase: GetAllFavoritePlacesUseCase by lazy {
        GetAllFavoritePlacesUseCase(placesRepository)
    }

    private val favoriteListViewModel: FavoritesViewModel by lazy {
        getViewModel { FavoritesViewModel(getAllFavoritePlacesUseCase) }
    }

    private val binding get() = _binding!!

    private lateinit var favoriteListAdapter: PlaceListAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentFavoritesBinding.inflate(inflater, container, false)
        val root: View = binding.root

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        favoriteListAdapter = PlaceListAdapter { place ->
            listener.openPlaceDetail(place)
        }
        favoriteListAdapter.setHasStableIds(true)

        binding.rvFavoriteVenues.run {
            setItemDecorationSpacing(resources.getDimension(R.dimen.list_item_padding))
            adapter = favoriteListAdapter
        }

        favoriteListViewModel.favoritePlacesList.observe(viewLifecycleOwner, Observer(favoriteListViewModel::onFavoritePlacesList))
        favoriteListViewModel.events.observe(viewLifecycleOwner, Observer(this::validateEvents))
    }

    private fun validateEvents(event: Event<FavoritesViewModel.FavoriteListNavigation>?) {
        event?.getContentIfNotHandled()?.let { navigation ->
            when (navigation) {
                is FavoritesViewModel.FavoriteListNavigation.ShowPlaceList -> navigation.run {
                    binding.tvListEmpty.isVisible = false
                    favoriteListAdapter.updateData(placesList)
                }
                FavoritesViewModel.FavoriteListNavigation.ShowEmptyListMessage -> {
                    binding.tvListEmpty.isVisible = true
                    favoriteListAdapter.updateData(emptyList())
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try{
            listener = context as OnFavoriteListFragmentListener
        }catch (e: ClassCastException){
            throw ClassCastException("$context must implement OnFavoriteListFragmentListener")
        }
    }

    interface OnFavoriteListFragmentListener {
        fun openPlaceDetail(place: Place)
    }

    companion object {

        fun newInstance(args: Bundle? = Bundle()) = FavoritesFragment().apply {
            arguments = args
        }
    }
}