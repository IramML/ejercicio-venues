package com.iramml.ejerciciovenues.database


import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.iramml.ejerciciovenues.database.converter.ListStringConverters
import com.iramml.ejerciciovenues.database.dao.PlaceDao
import com.iramml.ejerciciovenues.database.entity.PlaceEntity

@Database(entities = [PlaceEntity::class], version = 1)
@TypeConverters(ListStringConverters::class)
abstract class PlaceDatabase : RoomDatabase() {

    abstract fun placeDao(): PlaceDao

    companion object {

        private const val DATABASE_NAME = "venues_exercise_db"

        @Synchronized
        fun getDatabase(context: Context): PlaceDatabase = Room.databaseBuilder(
            context.applicationContext,
            PlaceDatabase::class.java,
            DATABASE_NAME
        ).build()
    }

}
