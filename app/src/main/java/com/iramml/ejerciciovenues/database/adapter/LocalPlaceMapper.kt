package com.iramml.ejerciciovenues.database.adapter

import com.iramml.ejerciciovenues.database.entity.PlaceEntity
import com.iramml.ejerciciovenues.domain.Place

fun List<PlaceEntity>.toPlacesDomainList() = map(PlaceEntity::toPlaceDomain)

fun PlaceEntity.toPlaceDomain() = Place(
    id,
    name,
    lat,
    lng
)

fun Place.toPlaceEntity() = PlaceEntity(
    id,
    name,
    lat,
    lng
)