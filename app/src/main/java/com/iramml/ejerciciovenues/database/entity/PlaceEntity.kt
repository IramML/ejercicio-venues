package com.iramml.ejerciciovenues.database.entity

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "place")
data class PlaceEntity(
    @PrimaryKey @ColumnInfo(name = "place_id") var id: String,
    @ColumnInfo(name = "place_name") var name: String,
    @ColumnInfo(name = "place_lat") var lat: Double,
    @ColumnInfo(name = "place_lng") var lng: Double,
)