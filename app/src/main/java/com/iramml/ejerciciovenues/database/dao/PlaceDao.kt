package com.iramml.ejerciciovenues.database.dao

import androidx.room.*
import com.iramml.ejerciciovenues.database.entity.PlaceEntity
import io.reactivex.Flowable
import io.reactivex.Maybe

@Dao
interface PlaceDao {

    @Query("SELECT * FROM Place")
    fun getAllFavoritePlaces(): Flowable<List<PlaceEntity>>

    @Query("SELECT * FROM Place WHERE place_id = :id")
    fun getPlaceById(id: String): Maybe<PlaceEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPlace(placeEntity: PlaceEntity)

    @Delete
    fun deletePlace(placeEntity: PlaceEntity)
}