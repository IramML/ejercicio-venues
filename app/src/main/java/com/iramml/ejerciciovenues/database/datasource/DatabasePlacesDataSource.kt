package com.iramml.ejerciciovenues.database.datasource

import com.iramml.ejerciciovenues.data.datasource.LocalPlacesDataSource
import com.iramml.ejerciciovenues.database.PlaceDatabase
import com.iramml.ejerciciovenues.database.adapter.toPlaceEntity
import com.iramml.ejerciciovenues.database.adapter.toPlacesDomainList
import com.iramml.ejerciciovenues.database.entity.PlaceEntity
import com.iramml.ejerciciovenues.domain.Place
import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class DatabasePlacesDataSource(
    database: PlaceDatabase
): LocalPlacesDataSource {

    private val placeDao by lazy { database.placeDao() }

    override fun getAllFavoritePlaces(): Flowable<List<Place>> = placeDao
        .getAllFavoritePlaces()
        .map(List<PlaceEntity>::toPlacesDomainList)
        .onErrorReturn { emptyList() }
        .subscribeOn(Schedulers.io())

    override fun getFavoritePlaceStatus(placeId: String): Maybe<Boolean> = placeDao
            .getPlaceById(placeId)
            .isEmpty
            .flatMapMaybe { isEmpty ->
                Maybe.just(!isEmpty)
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())


    override fun updateFavoritePlaceStatus(place: Place): Maybe<Boolean> {
        val placeEntity = place.toPlaceEntity()
        return placeDao.getPlaceById(placeEntity.id)
            .isEmpty
            .flatMapMaybe { isEmpty ->
                if(isEmpty){
                    placeDao.insertPlace(placeEntity)
                }else{
                    placeDao.deletePlace(placeEntity)
                }
                Maybe.just(isEmpty)
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
    }


}