package com.iramml.ejerciciovenues.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.LiveDataReactiveStreams
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.iramml.ejerciciovenues.domain.Place
import com.iramml.ejerciciovenues.presentation.util.Event
import com.iramml.ejerciciovenues.usecase.GetAllFavoritePlacesUseCase
import io.reactivex.disposables.CompositeDisposable

class FavoritesViewModel(
    private val getAllFavoritePlacesUseCase: GetAllFavoritePlacesUseCase
) : ViewModel(){

    private val disposable = CompositeDisposable()

    private val _events = MutableLiveData<Event<FavoriteListNavigation>>()
    val events: LiveData<Event<FavoriteListNavigation>> get() = _events

    val favoritePlacesList: LiveData<List<Place>>
        get() = LiveDataReactiveStreams.fromPublisher(getAllFavoritePlacesUseCase.invoke())

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }

    fun onFavoritePlacesList(favoritePlacesList: List<Place>) {
        if (favoritePlacesList.isEmpty()) {
            _events.value = Event(FavoriteListNavigation.ShowPlaceList(emptyList()))
            _events.value = Event(FavoriteListNavigation.ShowEmptyListMessage)
            return
        }

        _events.value = Event(FavoriteListNavigation.ShowPlaceList(favoritePlacesList))
    }

    sealed class FavoriteListNavigation {
        data class ShowPlaceList(val placesList: List<Place>) : FavoriteListNavigation()
        object ShowEmptyListMessage : FavoriteListNavigation()
    }

}
