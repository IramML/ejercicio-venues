package com.iramml.ejerciciovenues.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.android.gms.maps.model.LatLng
import com.iramml.ejerciciovenues.domain.Place
import com.iramml.ejerciciovenues.presentation.util.Event
import com.iramml.ejerciciovenues.usecase.GetNearbyPlaceUseCase
import io.reactivex.disposables.CompositeDisposable

class PlacesViewModel (
    private val getNearbyPlacesUseCase: GetNearbyPlaceUseCase
): ViewModel() {
    private val disposable = CompositeDisposable()
    private val _events = MutableLiveData<Event<PlacesListNavigation>>()
    val events: LiveData<Event<PlacesListNavigation>> get() = _events

    fun onGetNearbyPlaces(location: LatLng){
        disposable.add(
            getNearbyPlacesUseCase
                .invoke(location)
                .subscribe({ placeList ->
                    _events.value = Event(PlacesListNavigation.ShowPlacesList(placeList))
                }, { error ->

                    _events.value = Event(PlacesListNavigation.ShowPlacesError(error))
                })
        )
    }


    sealed class PlacesListNavigation {
        data class ShowPlacesError(val error: Throwable) : PlacesListNavigation()
        data class ShowPlacesList(val placesList: List<Place>) : PlacesListNavigation()
    }
}