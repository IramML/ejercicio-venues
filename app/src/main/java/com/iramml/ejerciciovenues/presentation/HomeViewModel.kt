package com.iramml.ejerciciovenues.presentation

import android.app.Activity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.android.gms.location.LocationResult
import com.google.android.gms.maps.model.LatLng
import com.iramml.ejerciciovenues.util.LocationListener
import com.iramml.ejerciciovenues.util.LocationUtil


class HomeViewModel() : ViewModel() {
    private var location: LocationUtil? = null
    private var currentLatLng: MutableLiveData<LatLng>? = null

    init {
        currentLatLng = MutableLiveData()
    }

    fun getCurrentLocation(): LiveData<LatLng?>? {
        return currentLatLng
    }

    fun initializeLocation(activity: Activity?) {
        location = LocationUtil(activity!!, object : LocationListener {
            override fun locationResponse(response: LocationResult?) {
                if (response != null) {
                    currentLatLng!!.setValue(LatLng(response.lastLocation.latitude, response.lastLocation.longitude))
                }
            }
        })
    }

    fun startLocationTracking() {
        location!!.initializeLocation()
    }

    fun stopLocationTracking() {
        location!!.stopUpdateLocation()
    }
}