package com.iramml.ejerciciovenues.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.iramml.ejerciciovenues.domain.Place
import com.iramml.ejerciciovenues.presentation.util.Event
import com.iramml.ejerciciovenues.usecase.GetFavoritePlaceStatusUseCase
import com.iramml.ejerciciovenues.usecase.UpdateFavoritePlaceStatusUseCase
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class PlaceDetailViewModel (
    private val place: Place? = null,
    private val getFavoritePlaceStatusUseCase: GetFavoritePlaceStatusUseCase,
    private val updateFavoritePlaceStatusUseCase: UpdateFavoritePlaceStatusUseCase,
) : ViewModel() {
    private val disposable = CompositeDisposable()

    private val _placeValues = MutableLiveData<Place>()
    val placeValues: LiveData<Place> get() = _placeValues

    private val _isFavorite = MutableLiveData<Boolean>()
    val isFavorite: LiveData<Boolean> get() = _isFavorite

    private val _events = MutableLiveData<Event<PlaceDetailNavigation>>()
    val events: LiveData<Event<PlaceDetailNavigation>> get() = _events


    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }

    fun onPlaceValidation() {
        if (place == null) {
            _events.value = Event(PlaceDetailNavigation.CloseActivity)
            return
        }

        _placeValues.value = place!!

        validateFavoritePlaceStatus(place.id)
    }

    fun onUpdateFavoritePlaceStatus() {
        disposable.add(
            updateFavoritePlaceStatusUseCase
                .invoke(place!!)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe { isFavorite ->
                    _isFavorite.value = isFavorite
                }
        )
    }

    private fun validateFavoritePlaceStatus(placeId: String){
        disposable.add(
            getFavoritePlaceStatusUseCase
                .invoke(placeId)
                .subscribe { isFavorite ->
                    _isFavorite.value = isFavorite
                }
        )
    }

    sealed class PlaceDetailNavigation {
        object CloseActivity : PlaceDetailNavigation()
    }
}