package com.iramml.ejerciciovenues.retrofit.adapter

import com.iramml.ejerciciovenues.domain.Place
import com.iramml.ejerciciovenues.retrofit.response.PlacesResponseServer

fun PlacesResponseServer.toPlacesDomainList(): List<Place> = results.map {
    it.run{
        Place(
            id,
            name,
            lat = geometry?.location?.lat ?: 0.0,
            lng = geometry?.location?.lng ?: 0.0,
        )
    }
}