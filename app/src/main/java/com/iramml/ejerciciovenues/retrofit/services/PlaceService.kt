package com.iramml.ejerciciovenues.retrofit.services

import com.iramml.ejerciciovenues.retrofit.APIConstants.ENDPOINT_PLACES
import com.iramml.ejerciciovenues.retrofit.response.PlacesResponseServer
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface PlaceService {
    @GET(ENDPOINT_PLACES)
    fun getNearbyPlaces(
        @Query("location") location: String,
        @Query("radius") radius: Int,
        @Query("key") key: String,
    ): Single<PlacesResponseServer>
}