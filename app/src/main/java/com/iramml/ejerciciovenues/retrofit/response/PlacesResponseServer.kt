package com.iramml.ejerciciovenues.retrofit.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.iramml.ejerciciovenues.retrofit.APIConstants.KEY_GEOMETRY
import com.iramml.ejerciciovenues.retrofit.APIConstants.KEY_ID
import com.iramml.ejerciciovenues.retrofit.APIConstants.KEY_LOCATION
import com.iramml.ejerciciovenues.retrofit.APIConstants.KEY_LOCATION_LAT
import com.iramml.ejerciciovenues.retrofit.APIConstants.KEY_LOCATION_LNG
import com.iramml.ejerciciovenues.retrofit.APIConstants.KEY_NAME
import com.iramml.ejerciciovenues.retrofit.APIConstants.KEY_PHOTOS
import com.iramml.ejerciciovenues.retrofit.APIConstants.KEY_PHOTO_REF
import com.iramml.ejerciciovenues.retrofit.APIConstants.KEY_RESULTS
import kotlinx.android.parcel.Parcelize

data class PlacesResponseServer(
    @SerializedName(KEY_RESULTS) val results: List<PlaceServer>
)

@Parcelize
data class PlaceServer(
    @SerializedName(KEY_ID) val id: String,
    @SerializedName(KEY_NAME) val name: String,
    @SerializedName(KEY_GEOMETRY) val geometry: GeometryServer?,
): Parcelable

@Parcelize
data class GeometryServer(
    @SerializedName(KEY_LOCATION) val location: LocationServer,
): Parcelable

@Parcelize
data class LocationServer(
    @SerializedName(KEY_LOCATION_LAT) val lat: Double,
    @SerializedName(KEY_LOCATION_LNG) val lng: Double,
): Parcelable

@Parcelize
data class PlacePhotosServer(
    @SerializedName(KEY_PHOTO_REF) val photoReference: String,
): Parcelable