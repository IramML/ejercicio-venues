package com.iramml.ejerciciovenues.retrofit

object APIConstants {
    const val BASE_API_URL = "https://maps.googleapis.com/maps/api/"

    const val ENDPOINT_PLACES = "place/nearbysearch/json?"

    const val KEY_RESULTS = "results"
    const val KEY_NAME = "name"
    const val KEY_PHOTOS = "photos"
    const val KEY_ID = "place_id"
    const val KEY_RATING = "rating"
    const val KEY_GEOMETRY = "geometry"
    const val KEY_LOCATION = "location"
    const val KEY_LOCATION_LAT = "lat"
    const val KEY_LOCATION_LNG = "lng"
    const val KEY_ICON = "icon"
    const val KEY_PHOTO_REF = "photo_reference"
}
