package com.iramml.ejerciciovenues.retrofit.request

import com.iramml.ejerciciovenues.retrofit.BaseRequest
import com.iramml.ejerciciovenues.retrofit.response.PlaceServer

class PlacesRequest(baseUrl: String): BaseRequest<PlaceServer>(baseUrl)