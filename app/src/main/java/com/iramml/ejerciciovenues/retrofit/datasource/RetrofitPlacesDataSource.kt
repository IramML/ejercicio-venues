package com.iramml.ejerciciovenues.retrofit.datasource

import com.google.android.gms.maps.model.LatLng
import com.iramml.ejerciciovenues.data.datasource.APIPlacesDataSource
import com.iramml.ejerciciovenues.domain.Place
import com.iramml.ejerciciovenues.retrofit.adapter.toPlacesDomainList
import com.iramml.ejerciciovenues.retrofit.request.PlacesRequest
import com.iramml.ejerciciovenues.retrofit.response.PlacesResponseServer
import com.iramml.ejerciciovenues.retrofit.services.PlaceService
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class RetrofitPlacesDataSource(
    private val placesRequest: PlacesRequest
): APIPlacesDataSource {
    override fun getNearbyPlaces(location: LatLng): Single<List<Place>> {
        return placesRequest
            .getService<PlaceService>()
            .getNearbyPlaces("${location.latitude},${location.longitude}", 1500, "AIzaSyAsrOw6mChNURX5NZNtjrn2cqJDZG2a8dM")
            .map(PlacesResponseServer::toPlacesDomainList)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
    }
}