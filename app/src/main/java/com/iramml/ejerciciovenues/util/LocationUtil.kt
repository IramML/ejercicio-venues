package com.iramml.ejerciciovenues.util

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.iramml.ejerciciovenues.R


class LocationUtil(var activity: Activity, val locationListener: LocationListener){
    private val permissionFineLocation=android.Manifest.permission.ACCESS_FINE_LOCATION
    private val permissionCoarseLocation=android.Manifest.permission.ACCESS_COARSE_LOCATION

    private val REQUEST_CODE_LOCATION=100

    private var fusedLocationClient:FusedLocationProviderClient?=null

    private var locationRequest:LocationRequest?=null
    private var callbabck:LocationCallback?=null
    init {
        initializeLocationRequest()
    }

    private fun initializeLocationRequest() {
        locationRequest= LocationRequest()
        locationRequest?.interval=50000
        locationRequest?.fastestInterval=5000
        locationRequest?.priority=LocationRequest.PRIORITY_HIGH_ACCURACY
    }
    private fun validatePermissionsLocation():Boolean{
        val fineLocationAvailable=
            ActivityCompat.checkSelfPermission(activity.applicationContext, permissionFineLocation)==PackageManager.PERMISSION_GRANTED
        val coarseLocationAvailable=ActivityCompat.checkSelfPermission(activity.applicationContext, permissionCoarseLocation)== PackageManager.PERMISSION_GRANTED

        return fineLocationAvailable && coarseLocationAvailable
    }
    private fun requestPermissions(){
        val contextProvider=ActivityCompat.shouldShowRequestPermissionRationale(activity, permissionFineLocation)

        if(contextProvider){
            Toast.makeText(activity, activity.getString(R.string.rationale_location), Toast.LENGTH_SHORT).show()
        }
        permissionRequest()
    }
    private fun permissionRequest(){
        ActivityCompat.requestPermissions(activity, arrayOf(permissionFineLocation, permissionCoarseLocation), REQUEST_CODE_LOCATION)
    }
    fun onRequestPermissionsResult(requestCode:Int, permissions:Array<out String>, grantResults:IntArray){
        when(requestCode){
            REQUEST_CODE_LOCATION->{
                if(grantResults.size>0 && grantResults[0]==PackageManager.PERMISSION_GRANTED){
                    getLocation()
                }else{
                    Toast.makeText(activity, activity.getString(R.string.permission_denied), Toast.LENGTH_SHORT).show()
                }
            }
        }
    }
    fun stopUpdateLocation(){
        this.fusedLocationClient?.removeLocationUpdates(callbabck)
    }
    fun initializeLocation(){
        if (validatePermissionsLocation()){
            getLocation()
        }else{
            requestPermissions()
        }
    }
    @SuppressLint("MissingPermission")
    private fun getLocation() {
        validatePermissionsLocation()
        fusedLocationClient = FusedLocationProviderClient(activity.applicationContext)
        callbabck = object: LocationCallback(){
            override fun onLocationResult(p0: LocationResult?) {
                super.onLocationResult(p0)
                locationListener.locationResponse(p0!!)
            }
        }
        fusedLocationClient?.requestLocationUpdates(locationRequest, callbabck, null)
    }
}

interface LocationListener {
    fun locationResponse(response: LocationResult?)
}
