package com.iramml.ejerciciovenues.util

import com.iramml.ejerciciovenues.BuildConfig

object Constants {

    const val EXTRA_PLACE = "${BuildConfig.APPLICATION_ID}.EXTRA_PLACE"

}