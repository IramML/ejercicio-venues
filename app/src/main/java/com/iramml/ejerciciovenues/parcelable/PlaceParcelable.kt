package com.iramml.ejerciciovenues.parcelable

import android.os.Parcelable
import com.iramml.ejerciciovenues.domain.Place
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PlaceParcelable (
    val id: String,
    val name: String,
    val lat: Double,
    val lng: Double
) : Parcelable


fun Place.toPlaceParcelable() = PlaceParcelable(
    id,
    name,
    lat,
    lng
)


fun PlaceParcelable.toPlaceDomain() = Place(
    id,
    name,
    lat,
    lng
)
